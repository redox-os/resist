# ReSIST - Redox System Interface Specifications and Tests

This repository contains specifications of the expected system interfaces for
the specified version of Redox, as well as tests to ensure those interfaces
are present. The master branch may contain a work-in-progress version.
